
module.exports = function(config) {
    config.set({

        basePath: '../',
        frameworks: ['jasmine'],
        files: [
            'bower_components/angular/angular.js',
            'node_modules/angular-mocks/angular-mocks.js',
            'test/unit/**/*.js',
            'app/scripts/*.js'
        ],

        // list of files to exclude
        exclude: ['test/protractor.conf.js'],
        preprocessors: {},
        reporters: ['progress'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: false,
        browsers: ['Chrome'],
        singleRun: true
    });
};