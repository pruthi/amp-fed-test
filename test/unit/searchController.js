describe('Controller: SearchController', function () {

  // load the controller's module
  beforeEach(module('testApp'));

  var SearchController, scope, $httpBackend;
  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, nameFactory) {


    scope = $rootScope.$new();
    SearchController = $controller('SearchController', {
      $scope: scope, nameFactory: nameFactory
    });
//            $httpBackend.flush();

  }));
    

  it('should fetch 6 names with required fields', function(){
      expect(scope.names).toBeDefined();
      expect(scope.names.length).toBe(6);

  });

  it('should have the correct data order', function() {

      expect(scope.names[0].firstName).toBe("Sean");
      expect(scope.names[1].Title).toBe("AEM Magician");

  });
    
});